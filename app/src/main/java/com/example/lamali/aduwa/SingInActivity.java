package com.example.lamali.aduwa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SingInActivity extends AppCompatActivity {
    public Button b1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_in);
        b1=(Button) findViewById(R.id.btnSignIn);
        b1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openselectcanteen();
            }
        });
    }
    public void openselectcanteen(){
        Intent intent=new Intent(this,SelectCanteenActivity.class);
        startActivity(intent);
    }
}
